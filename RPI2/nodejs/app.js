// Requires
var express = require('express');
var path = require('path');
var querystring = require("querystring");
var url = require('url');
var sys = require('util');
var exec = require('child_process').exec;
var child;

// Create app
var app = express();
//var port = 3700;
var port = 80;

// Set views
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'views')));

// Serve files
app.get('/interface', function(request, response){
  response.sendFile(__dirname + '/views/interface.html')
});

// Send commands to PI
app.get("/send", function(request, response){
    
    // Get data
    var queryData = url.parse(request.url, true).query;
    console.log("Requisicao recebida: " + queryData.state + ".");
    var request = generateRequest(queryData.state);
    console.log("Requisicao codificada: " + request + ".");

    var cmd_output = "";

    child = exec("/home/pi/rf24-sec/rpi-rf24 " + request, function (error, stdout, stderr) {
      console.log('Saida do programa: ' + stdout);
      if (error !== null) {
        console.log('Erro: ' + stderr);
        console.log('Erro execucao: ' + error);
        cmd_output = 'Erro: ' + error;
      }
      else {
        cmd_output = stdout;
      }

      // Resposta            
      var oMyObject = { cmd_out: cmd_output };        
      response.type('application/json');
      response.send(oMyObject);
      response.end();
    });
});

// Start server
app.listen(port);
console.log("Aguardando conexoes na porta " + port);

function generateRequest(request)
{
  switch(request) {
    case "on_r":
      return  "0" + randomize();
    case "off_r":
      return  "1" + randomize();
    case "on_g":
      return  "2" + randomize();
    case "off_g":
      return  "3" + randomize();
    case "on_b":
      return  "4" + randomize();
    case "off_b":
      return  "5" + randomize();
    default:
      return "6" + randomize();
  }
}

function randomize()
{
  var ret = "";
  for (var i = 0; i < 15; i++)
  {
      ret = ret + "" + Math.floor((Math.random() * 9) + 1);
  }
  return ret;
}