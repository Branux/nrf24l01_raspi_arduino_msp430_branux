#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <RF24/RF24.h>

using namespace std;

// Setup for GPIO 15 CE and CE0 CSN with SPI Speed @ 8Mhz
RF24 radio(RPI_V2_GPIO_P1_15, BCM2835_SPI_CS0, BCM2835_SPI_SPEED_8MHZ);

const uint64_t linkReading = 0xE7D3F035FF;            // Este e o endereco de recepcao
const uint64_t linkWriting = 0xDEADBEEF02;            // Este e o endereco para onde estou transmitindo

const int max_payload_size = 16;                      // Tamanho máximo da mensagem (defaul é 32)
unsigned char receive_payload[max_payload_size + 1];  // +1 to allow room for a terminating NULL char
unsigned long max_time = 500;                         // Tempo para timeout
const int max_amount_timeout = 10;                    // Quantidade máxima de timeouts permitidos
int amount_timeout = 0;                               // Contador de timeouts

void init_radio()
{
    // Iniciando e configuranco o radio
    radio.begin();
    
    // Definindo o canal
    radio.setChannel(76);
    
    // Definindo a velocidade da transmissão
    radio.setDataRate(RF24_250KBPS);        // Pode ser também RF24_1MBPS
    
    // Opcionalmente, incrementa o tempo entre as tentativas
    radio.setRetries(15, 15);

    radio.setPALevel(RF24_PA_MAX);
    radio.enableDynamicPayloads();
    radio.setCRCLength(RF24_CRC_16);

    radio.openWritingPipe(linkWriting);
    radio.openReadingPipe(1, linkReading);
}

int main(int argc, char** argv)
{
    if (argc <= 1)
    {
        cout << "=================================================================================" << endl
             << "Uso: " << argv[0] << " <on_|off_> (liga e desliga) + <r|g|b> (Red, green e blue)." << endl
             << "Exemplo: " << argv[0] << " on_r (liga o led vermelho)" << endl
             << "=================================================================================" << endl;
        exit(1);
    }
    
    // Inciando o módulo de RF.
    init_radio();
    
    // Enviando para o MSP o primeiro argumento recebido
    unsigned char *send_payload = (unsigned char*)argv[1];
    
    // Tentando enquanto não estoura a quantidade máxima de timeouts
    while (amount_timeout < max_amount_timeout)
    {            
        // Primeiro, para a escuta para poder transmitir.
        radio.stopListening();
        
        // Agora envia. O radio fica "travado" até que a transmissão seja completada.
        radio.write(send_payload, strlen((char*)send_payload));
        
        // Agora volta a escutar...
        radio.startListening();
        
        // Espera a resposta ou timeout.
        unsigned long started_waiting_at = millis();
        bool timeout = false;
        while (!radio.available() && !timeout)
        {
            if (millis() - started_waiting_at > max_time )
            {
                timeout = true;
                amount_timeout++;
            }   
        }
        
        // Retorna o resultado
        if (!timeout)
        {
            // Pega a resposta e compara.
            uint8_t len = radio.getDynamicPayloadSize();
            radio.read(receive_payload, len);
            
            // Coloca um 0 no final para facilitar a impressão em tela.
            receive_payload[len] = 0;
            
            // Imprime a resposta
            printf("%s\n\r", receive_payload);
            
            exit(0);
        }
        
        // Espera um pouco para tentar novamente.
        delay(100);
    }
    
    // Se chegou aqui é porque teve timeout.
    printf("O dispositivo remoto não respondeu\n\r");
    
    return 0;
}
